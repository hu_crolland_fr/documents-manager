<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class User.
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 *
 * @author Rolland Csatari <rolland.csatari@ekino.com>
 */
class User
{
    /**
     *  The following role can has all rights.
     */
    const USER_ADMIN_ROLE = 'USER_ADMIN_ROLE';

    /**
     * The following role can only upload files.
     */
    const USER_UPLOAD_ROLE = 'USER_UPLOAD_ROLE';

    /**
     * The following role can only download files.
     */
    const USER_DOWNLOAD_ROLE = 'USER_DOWNLOAD_ROLE';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    public function __toString()
    {
        return $this->getUsername() ?? '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}

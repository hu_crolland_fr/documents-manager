<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Mapper;

use App\Entity\Category;
use App\Entity\Document;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class CategoryListMapper.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class CategoryListMapper
{
    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function map(?Category $category): array
    {
        $results = [];
        if (!$category instanceof Category) {
            return $results;
        }

        $results['id'] = $category->getId();
        $results['name'] = $category->getName();

        $documents = [];
        foreach ($category->getDocuments() as $document)
        {
            /* @var Document $document */
            $documents[] = [
                'id'           => $document->getId(),
                'name'         => $document->getName(),
                'version'      => $document->getVersion(),
                'href'         => $this->urlGenerator->generate('frontend_download_document', ['id' => $document->getId()]),
                'uploadedAt'   => $document->getUploadedAt(),
            ];
        }

        $results['documents'] = $documents;

        return $results;
    }
}

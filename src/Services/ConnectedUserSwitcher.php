<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Services;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ConnectedUserSwitcher.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class ConnectedUserSwitcher
{
    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(SessionInterface $session, UserRepository $userRepository)
    {
        $this->session = $session;
        $this->userRepository = $userRepository;
    }

    public function switchUserByUserId(int $userId): void
    {
        $user = $this->userRepository->find($userId);
        if (!$user instanceof User) {
            $user = $this->userRepository->findOneBy([], []);
        }

        $this->session->set('connectedUser', $user);
    }

    public function getConnectedUser(): ?User
    {
        $user = $this->session->get('connectedUser');

        return $user instanceof User ? $user : null;
    }

    public function getAvailableUsers(): array
    {
        return $this->userRepository->findAll();
    }
}

<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class CategoryFormBuilder.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class FormBuilder
{
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    /**
     * @var FlashBagInterface
     */
    protected $flashBag;

    /**
     * @var ObjectManager
     */
    protected $manager;

    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        FlashBagInterface $flashBag,
        FormFactoryInterface $formFactory,
        ManagerRegistry $managerRegistry
    ) {
        $this->formFactory = $formFactory;
        $this->urlGenerator = $urlGenerator;
        $this->flashBag = $flashBag;
        $this->manager = $managerRegistry->getManager();
    }

    public function applyFormHandler(string $entityClass, string $entityType, string $formAction, array $formActionParams, object $object, Request $request, string $successMsg = ''): void
    {
        if ($entityClass !== \get_class($object)) {
            throw new \Exception(sprintf('Instance of %s was required, %s given.', $entityClass, \get_class($object)));
        }

        try {
            $form = $this->formFactory->create($entityType, $object,
                ['formAction' => $this->urlGenerator->generate($formAction, $formActionParams)]
            );

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $object = $this->interfereObjectInFormHandler($form->getData());

                $this->manager->persist($object);
                $this->manager->flush();

                $this->flashBag->add('success', $successMsg);
            }

        } catch (\Exception $exception) {
            $this->flashBag->add('error', $exception->getMessage());
        }
    }

    public function interfereObjectInFormHandler(object $object): object
    {
        return $object;
    }
}

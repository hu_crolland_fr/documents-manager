<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Services;

use App\Entity\Category;
use App\Entity\Document;
use App\Repository\DocumentRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class DocumentUploader.
 *
 * @author Rolland Csatari <rolland.csatari@ekino.com>
 */
class DocumentUploader
{
    const FILE_NAME_TPL = '%s_[v%d].%s';

    /**
     * @var string
     */
    protected $defaultUploadPath;

    /**
     * @var DocumentRepository
     */
    protected $documentRepository;

    public function __construct(DocumentRepository $documentRepository, ParameterBagInterface $parameterBag)
    {
        $this->defaultUploadPath = sprintf('%s/public/uploads/', $parameterBag->get('kernel.project_dir'));
        $this->documentRepository = $documentRepository;
    }

    public function upload(Document $document): Document
    {
        $this->generateFileName($document);

        $document->getFile()->move($this->defaultUploadPath,  $document->getOriginalName());

        return $document;
    }

    public function downloadLinkBuilder(string $originalFileName): string
    {
        return sprintf('%s%s', $this->defaultUploadPath, $originalFileName);
    }

    public function remove(Category $category): void
    {
        /* @var Document $document */
        foreach ($category->getDocuments() as $document) {
            \unlink(sprintf('%s%s', $this->defaultUploadPath, $document->getOriginalName()));
        }
    }

    protected function generateFileName(Document $document): Document
    {
        $fileName = sprintf(static::FILE_NAME_TPL,
            $document->getFile()->getClientOriginalName(),
            1, // file version
            $document->getFile()->getClientOriginalExtension()
        );

        /* @var Document $result */
        $result = $this->documentRepository->findOneBy(['originalName' => $fileName]);
        $currentFileVersion = $result instanceof Document ? $document->getVersion() + 1 : 1;

        $document->setOriginalName(sprintf(static::FILE_NAME_TPL,
            $document->getFile()->getClientOriginalName(),
            $currentFileVersion,
            $document->getFile()->getClientOriginalExtension()
        ));

        $document->setVersion($currentFileVersion);

        return $document;
    }
}

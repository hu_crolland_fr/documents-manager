<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Services;

use App\Entity\Document;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class DocumentFormBuilder.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class DocumentFormBuilder extends FormBuilder
{
    /**
     * @var DocumentUploader
     */
    protected $documentUploader;

    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        FlashBagInterface $flashBag,
        FormFactoryInterface $formFactory,
        ManagerRegistry $managerRegistry,
        DocumentUploader $documentUploader
    ) {
        parent::__construct($urlGenerator, $flashBag, $formFactory, $managerRegistry);

        $this->documentUploader = $documentUploader;
    }

    public function interfereObjectInFormHandler(object $object): object
    {
        if (!$object instanceof Document) {
            throw new \Exception(sprintf('Instance of %s was required, %s given.', Document::class, \get_class($object)));
        }

        return $this->documentUploader->upload($object);
    }
}

<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20200818205732.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
final class Version20200821212010 extends AbstractMigration
{
    private const CATEGORY_DATA = [
        ['parent_id' => null, 'name' => 'First Parent Category'],
        ['parent_id' => null, 'name' => 'Second Parent Category'],
        ['parent_id' => 1, 'name' => '[FIRST-CATEGORY] : Child category 1'],
        ['parent_id' => 1, 'name' => '[FIRST-CATEGORY] : Child category 2'],
        ['parent_id' => 2, 'name' => '[SECOND-CATEGORY] : Child category 1'],
    ];

    private const USERS_DATA = [
        ['username' => 'USER [ADMIN]', 'roles' => ['USER_ADMIN_ROLE']],
        ['username' => 'USER [UPLOAD]', 'roles' => ['USER_UPLOAD_ROLE']],
        ['username' => 'USER [DOWNLOAD]', 'roles' => ['USER_DOWNLOAD_ROLE']],
        ['username' => 'USER [UPLOAD/DOWNLOAD]', 'roles' => ['USER_ADMIN_ROLE', 'USER_DOWNLOAD_ROLE']],
    ];

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /** ===============================================================
         *  Init database schema.
         * ============================================================= */

        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_64C19C1727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, original_name VARCHAR(255) NOT NULL, version INT NOT NULL, uploaded_at DATETIME DEFAULT NULL, INDEX IDX_D8698A7612469DE2 (category_id), INDEX IDX_D8698A76A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A7612469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');

        /** ===============================================================
         *  Insert some categories.
         * ============================================================= */

        foreach (static::CATEGORY_DATA as $categoryDatum) {
            $this->addSql('INSERT into category (parent_id, name) VALUES (:parent_id, :name)', [
                ':parent_id' => $categoryDatum['parent_id'],
                ':name'      => $categoryDatum['name'],
            ]);
        }

        /** ===============================================================
         *  Insert some users.
         * ============================================================= */

        foreach (static::USERS_DATA as $userDatum) {
            $this->addSql('INSERT into user (username, roles) VALUES (:username, :roles)', [
                ':username' => $userDatum['username'],
                ':roles'    => \json_encode($userDatum['roles']),
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM category');
        $this->addSql('DELETE FROM user');

        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A7612469DE2');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76A76ED395');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE user');
    }
}

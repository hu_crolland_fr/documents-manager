<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Twig;

use App\Entity\Category;
use App\Entity\Document;
use App\Entity\User;
use App\Type\CategoryType;
use App\Type\DocumentType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class DocumentManagerFormBuilder.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class FormBuilderExtension extends AbstractExtension
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(FormFactoryInterface $formFactory, UrlGeneratorInterface $urlGenerator)
    {
        $this->formFactory = $formFactory;
        $this->urlGenerator = $urlGenerator;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('buildCategoryForm', [$this, 'buildCategoryForm']),
            new TwigFunction('buildDocumentUploaderForm', [$this, 'buildDocumentUploaderForm']),
        ];
    }

    public function buildCategoryForm(?Category $category = null): FormView
    {
        $actionPath = [];

        if (!$category instanceof Category) {
            $category = new Category();
            $action = CategoryType::ADD_ACTION;
        } else {
            $action = CategoryType::EDIT_ACTION;
            $actionPath = ['id' => $category->getId()];
        }

        return $this->formFactory->create(CategoryType::class, $category, [
            'formAction' => $this->urlGenerator->generate($action, $actionPath)]
        )->createView();
    }

    public function buildDocumentUploaderForm(Category $category, User $user): FormView
    {
        return $this->formFactory->create(DocumentType::class, new Document(), [
            'formAction' => $this->urlGenerator->generate(DocumentType::ADD_ACTION, [
                'id'      => $category->getId(),
                'user_id' => $user->getId(),
            ])]
        )->createView();
    }
}

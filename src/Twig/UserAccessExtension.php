<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Twig;

use App\Entity\User;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class UserAccessExtension.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class UserAccessExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('userCanAccess', [$this, 'userCanAccess'])
        ];
    }

    public function userCanAccess(string $requestedRole, User $user): bool
    {
        if (\in_array(User::USER_ADMIN_ROLE, $user->getRoles())) {
            return true;
        }

        return \in_array($requestedRole, $user->getRoles());
    }
}

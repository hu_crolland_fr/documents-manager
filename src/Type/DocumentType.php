<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Type;

use App\Entity\Document;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DocumentType.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class DocumentType extends AbstractType
{
    const ADD_ACTION = 'frontend_add_document';
    const EDIT_ACTION = 'frontend_edit_document';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            /* Set form action. */
            ->setAction($options['formAction'])

            /* Add form fields */
            ->add('name', TextType::class, [
                'required' => true,
                'label'    => 'Name',
            ])
            ->add('file', FileType::class)
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-outline-success',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault('data', new Document())
            ->setRequired('formAction')
        ;
    }
}

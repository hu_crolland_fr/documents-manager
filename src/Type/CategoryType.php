<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Type;

use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CategoryType.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class CategoryType extends AbstractType
{
    const ADD_ACTION = 'frontend_add_category';
    const EDIT_ACTION = 'frontend_edit_category';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /* @var Category $category */
        $category = $builder->getData();

        /* Disable the parent selector when the category is root in edit action. */
        $rootCategoryInEditAction = \is_null($category->getParent()) && $options['formAction'] === static::EDIT_ACTION;

        $builder
            /* Set form action. */
            ->setAction($options['formAction'])

            /* Add form fields */
            ->add('name', TextType::class, [
                'required' => true,
                'label'    => 'Name',
            ])
            ->add('parent', EntityType::class, [
                'class'        => Category::class,
                'choice_label' => 'name',
                'placeholder'  => 'Choose an option',
                'required'     => false,
                'disabled'     => $rootCategoryInEditAction,
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-outline-success',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault('data', new Category())
            ->setRequired('formAction')
        ;
    }
}

<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Category;
use App\Mapper\CategoryListMapper;
use App\Services\DocumentUploader;
use App\Services\FormBuilder;
use App\Type\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController.
 *
 * @Route("/category")
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class CategoryController extends AbstractController
{
    /**
     * @var FormBuilder
     */
    protected $formBuilder;

    /**
     * @var CategoryListMapper
     */
    protected $categoryListMapper;

    /**
     * @var DocumentUploader
     */
    protected $documentUploader;

    public function __construct(FormBuilder $formBuilder, CategoryListMapper $categoryListMapper, DocumentUploader $documentUploader)
    {
        $this->formBuilder = $formBuilder;
        $this->categoryListMapper = $categoryListMapper;
        $this->documentUploader = $documentUploader;
    }

    /**
     * @Route("/{id}/documents", name="frontend_category_by_category_id", methods={"GET"})
     */
    public function getDocumentsByCategory(?Category $category): JsonResponse
    {
        return new JsonResponse($this->categoryListMapper->map($category));
    }

    /**
     * @Route("/add", name="frontend_add_category", methods={"POST"})
     */
    public function addNewCategory(Request $request): RedirectResponse
    {
        $this->formBuilder->applyFormHandler(
            Category::class,
            CategoryType::class,
            CategoryType::ADD_ACTION,
            [],
            new Category(),
            $request,
            'The new category was added successfully.'
        );

        return $this->redirectToRoute('frontend_home_page');
    }

    /**
     * @Route("/edit/{id}", name="frontend_edit_category", methods={"PUT", "POST"})
     */
    public function editCategory(?Category $category, Request $request)
    {
        if (!$category instanceof Category) {
            $this->addFlash('error', 'The requested category was not found.');

            return $this->redirectToRoute('frontend_home_page');
        }

        $this->formBuilder->interfereObjectInFormHandler(function (): object {
            return new Category();
        });

        $this->formBuilder->applyFormHandler(
            Category::class,
            CategoryType::class,
            CategoryType::EDIT_ACTION,
            ['id' => $category->getId()],
            $category,
            $request,
            'The category was updated successfully.'
        );

        return $this->redirectToRoute('frontend_home_page');
    }

    /**
     * @Route("/{id}/remove", name="frontend_remove_category")
     */
    public function removeCategory(?Category $category): RedirectResponse
    {
        if (!$category instanceof Category) {
            $this->addFlash('error', 'The deletion of category was failed. The requested category was not found.');

            return $this->redirectToRoute('frontend_home_page');
        }

        try {
            $this->documentUploader->remove($category);

            $this->getDoctrine()->getManager()->remove($category);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'The category was removed successfully.');

        } catch (\Exception $exception) {
            $this->addFlash('error', sprintf('The deletion of category was failed. %s', $exception->getMessage()));
        }

        return $this->redirectToRoute('frontend_home_page');
    }
}

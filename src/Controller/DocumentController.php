<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Document;
use App\Entity\User;
use App\Repository\DocumentRepository;
use App\Services\ConnectedUserSwitcher;
use App\Services\DocumentFormBuilder;
use App\Services\DocumentUploader;
use App\Type\DocumentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

/**
 * Class DocumentController.
 *
 * @Route("/document")
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class DocumentController extends AbstractController
{
    /**
     * @var DocumentUploader
     */
    protected $documentUploader;

    /**
     * @var ConnectedUserSwitcher
     */
    protected $connectedUserSwitcher;

    /**
     * @var DocumentRepository
     */
    protected $documentRepository;

    /**
     * @var DocumentFormBuilder
     */
    protected $documentFormBuilder;

    public function __construct(DocumentUploader $documentUploader, ConnectedUserSwitcher $connectedUserSwitcher, DocumentRepository $documentRepository, DocumentFormBuilder $documentFormBuilder)
    {
        $this->documentUploader = $documentUploader;
        $this->connectedUserSwitcher = $connectedUserSwitcher;
        $this->documentRepository = $documentRepository;
        $this->documentFormBuilder = $documentFormBuilder;
    }

    /**
     * @Route("/add/category/{id}/user/{user_id}", name="frontend_add_document", methods={"POST"})
     * @Entity("user", expr="repository.find(user_id)")
     */
    public function addNewDocument(Category $category, User $user, Request $request): RedirectResponse
    {
        if (\is_null($category->getParent())) {
            $this->addFlash('error', 'You cannot upload documents in the root category.');

            return $this->redirectToRoute('frontend_home_page');
        }

        $document = new Document();
        $document->setCategory($category);
        $document->setUser($user);

        $this->documentFormBuilder->applyFormHandler(
            Document::class,
            DocumentType::class,
            DocumentType::ADD_ACTION,
            ['id' => $category->getId(), 'user_id' => $user->getId()],
            $document,
            $request,
            'The new document was uploaded successfully.'
        );

        return $this->redirectToRoute('frontend_home_page');
    }

    /**
     * @Route("/{id}/download", name="frontend_download_document", methods={"GET"})
     */
    public function downloadDocument(?Document $document): BinaryFileResponse
    {
        if (!$document instanceof Document) {
            throw new BadRequestHttpException();
        }

        $fullPath = $this->documentUploader->downloadLinkBuilder($document->getOriginalName());

        $response = new BinaryFileResponse($fullPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $document->getOriginalName());

        return $response;
    }
}

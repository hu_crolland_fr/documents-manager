<?php

declare(strict_types=1);

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Services\ConnectedUserSwitcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController.
 *
 * @author Rolland Csatari <rolland.csatari@ekino.com>
 *
 * @Route("/")
 */
class HomeController extends AbstractController
{
    /**
     * @var ConnectedUserSwitcher
     */
    protected $connectedUserSwitcher;

    public function __construct(ConnectedUserSwitcher $connectedUserSwitcher)
    {
        $this->connectedUserSwitcher = $connectedUserSwitcher;

        /* Init the default connected user. */
        $this->connectedUserSwitcher->switchUserByUserId(0);
    }

    /**
     * @Route("/", name="frontend_home_page", methods={"GET"})
     */
    public function index(Request $request, CategoryRepository $categoryRepository): Response
    {
        /* Switch the current connected user by user id. */
        $this->connectedUserSwitcher->switchUserByUserId((int) $request->get('userId'));

        return $this->render('home/index.html.twig', [
            'elements'          => $categoryRepository->findBy(['parent' => null]),
            'availableUsers'    => $this->connectedUserSwitcher->getAvailableUsers(),
            'schemeAndHttpHost' => $request->getSchemeAndHttpHost(),
        ]);
    }
}

### Document Manager

#### Installation with DOCKER

You can get a list of all make commands with:
```
make : All make commands are executed in docker containers.
```

Install the project:
```
make make infra-up          : Build docker infrastructure
env=dev make app-install    : Install the app (front/back)
env=dev make app-data-reset : Init the database.
```

Add hosts to your /etc/hosts file:

```
127.0.0.1 http://document-manager.localdev
```

### Install without DOCKER

Required tools: **Nginx**, **PHP** (7.2.*|7.3.*), **MySQL**, **Node**, **Yarn**

Install the project:
```
composer install                       : Install back dependencies
bin/console doctrine:database:create   : Create the database
bin/console doctrine:migration:migrate : Init the database

yarn install                           : Install front dependencies
yarn encore dev                        : Compile front assets
```

Add hosts to your /etc/hosts file:

```
127.0.0.1 http://document-manager.localdev
```

#### Urls

**Locale :** ``http://document-manager.localdev/`` <br />
**Online :** ``http://documents-manager.crolland.fr/``

### User GUIDE

#### About user roles
```
Document Manager has 4 users by default. The connected user can be changed
with the "Switch User" selector in the navbar. 

Each user has own roles: 
  - USER [ADMIN]           : Has all rights.
  - USER [UPLOAD]          : Can add new categories and can upload files and remove categories.
  - USER [DOWNLOAD]        : Can only download files.
  - USER [UPLOAD/DOWNLOAD] : Can add new categories upload and download fiels and remove categories. 
```

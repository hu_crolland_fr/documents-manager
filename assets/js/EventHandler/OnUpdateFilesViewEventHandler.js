/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This function update the selected category label
 * in the top of the documents list.
 *
 * @param {string} categoryName
 *
 * @return {void}
 */
const updateSelectedCategoryLabel = categoryName => {
    const selectedCategoryLabel = document.getElementById('selected-category-label');
    selectedCategoryLabel.innerText = categoryName;
};

/**
 * This function clear the document list in the table.
 *
 * @param {object} selectCategoryDocumentsDOM
 *
 * @return {void}
 */
const clearDocumentListBeforeUpdate = selectCategoryDocumentsDOM => {
    const selectCategoryFileListDOMChildren = document.querySelectorAll('#selected-category-file-list > tr');

    selectCategoryFileListDOMChildren.forEach(child => {
        selectCategoryDocumentsDOM.removeChild(child);
    });
};

/**
 * This function add the document download link in function
 * of current user rights.
 *
 * @param {string} href
 * @param {object} trElement
 * @param {object} selectCategoryDocumentsDOM
 *
 * @return {void}
 */
const addDocumentDownloadColumn = (href, trElement, selectCategoryDocumentsDOM) => {
    const userHasDownloadRights = selectCategoryDocumentsDOM.getAttribute('data-user-has-download-rights');

    const tdElement = document.createElement('td');
    const iElement = document.createElement('i');
    const aElement = document.createElement('a');

    iElement.setAttribute('class', 'fa fa-download');
    iElement.setAttribute('aria-hidden', 'true');

    if (userHasDownloadRights === '1') {
        aElement.setAttribute('href', href);
    } else {
        aElement.setAttribute('href', '#');
        aElement.setAttribute('style', 'pointer-events: none; color: #212529;')
    }

    aElement.appendChild(iElement);
    tdElement.appendChild(aElement);
    trElement.appendChild(tdElement);
};

/**
 * This function generate a new row into the document list table.
 *
 * @param {object} item
 * @param {object} selectCategoryDocumentsDOM
 *
 * @return {void}
 */
const generateDocumentRow = (item, selectCategoryDocumentsDOM) => {
    const trElement = document.createElement('tr');

    /* Iterate on current object to generate the different columns. */
    for (const key in item) {

        /* Exclude the document href. This was added below in function of current user rights. */
        if (key === 'href') {
            continue;
        }

        /* Build the new row column. */
        const tdElement = document.createElement('td');
        tdElement.innerText = item[key];

        trElement.appendChild(tdElement);
    }

    /* Generate document download column. */
    addDocumentDownloadColumn(item['href'], trElement, selectCategoryDocumentsDOM);

    /* Push the new row into the document list. */
    selectCategoryDocumentsDOM.appendChild(trElement);
};

/**
 * This function generate the document list of the selected category.
 *
 * @param {object} event
 *
 * @return {void}
 */
export default function (event) {
    const category = event.detail;
    const selectCategoryDocumentsDOM = document.getElementById('selected-category-file-list');

    /* Update the selected category label in the top of the document list. */
    updateSelectedCategoryLabel(category['name']);

    /* Clear the document list before update if. */
    clearDocumentListBeforeUpdate(selectCategoryDocumentsDOM);

    /* Update the category list indicator. */
    const emptyDocumentListText = document.querySelectorAll('#empty-document-list-text');
    emptyDocumentListText.forEach(element => {
        element.innerHTML = category['documents'].length > 0 ? '' : 'The selected category does not contain associated files.';
    });

    /* Build the document rows. */
    category['documents'].map(document => generateDocumentRow(document, selectCategoryDocumentsDOM));
}

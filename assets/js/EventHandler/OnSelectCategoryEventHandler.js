/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import eventDispatcher from '../Helper/EventDispatcher'
import { onUpdateFilesViewEvent } from '../Constants/Constants'

/**
 * This function request the selected category documents
 * and notify the onUpdateFilesViewEvent to update the file lists.
 *
 * @param {object} event
 *
 * @return {void}
 */
export default function (event) {
    const categoryDocumentsRequestURL = event.detail['pathToDocuments'];

    /* Build prepare request. */
    const headers = new Headers();
    const requestInit = { method: 'GET', headers: headers, mode: 'cors', cache: 'default' };
    const request = new Request(categoryDocumentsRequestURL, requestInit);

    /* Get category documents and notify the onUpdateFilesViewEvent. */
    window
        .fetch(request, requestInit)
        .then((response) => { return response.json() })
        .then((response) => { eventDispatcher(onUpdateFilesViewEvent, response) })
    ;
}

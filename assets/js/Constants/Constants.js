/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/** ===========================
 * Event names.
 * ============================ */

export const onUpdateFilesViewEvent = 'onUpdateFilesViewEvent';
export const onSelectCategoryEvent = 'onSelectCategoryEvent';

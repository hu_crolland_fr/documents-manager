/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Event Dispatcher helper.
 *
 * @param {string} eventName
 * @param {object} data
 *
 * @return {void}
 */
export default function (eventName, data) {
    const newCustomEvent = new CustomEvent(eventName, {'detail': data});
    document.dispatchEvent(newCustomEvent);
}

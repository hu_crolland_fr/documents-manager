/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import onSelectCategoryEventHandler from '../EventHandler/OnSelectCategoryEventHandler'
import onUpdateFilesViewEventHandler from '../EventHandler/OnUpdateFilesViewEventHandler'
import { onSelectCategoryEvent, onUpdateFilesViewEvent } from '../Constants/Constants'

document.addEventListener(onSelectCategoryEvent, onSelectCategoryEventHandler);
document.addEventListener(onUpdateFilesViewEvent, onUpdateFilesViewEventHandler);

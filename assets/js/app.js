/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/** =============================
 *  Import stylesheets
 * ============================== */

/* External */
import 'font-awesome/css/font-awesome.css'
import 'bootstrap/dist/css/bootstrap.css'

/* Custom */
import '../css/App.scss'

/** =============================
 *  Import Javascript Components
 * ============================== */

/* Custom */
import './Helper/CategoryTree'
import './EventListener/BaseEventListener';

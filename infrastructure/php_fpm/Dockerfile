FROM php:7.3-fpm-alpine

ARG env="local"
ARG uid=82
ARG xdebug_remote_host="192.168.33.1"
ARG xdebug_version=2.9.1

# icu-dev is required for installing intl
RUN echo "@edge-main http://nl.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories \
    && echo "@edge-testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && echo "@edge-community http://nl.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \

    && apk add --update --upgrade alpine-sdk autoconf curl git icu-dev libjpeg-turbo-dev libpng-dev libzip-dev make \
    && docker-php-ext-configure gd --with-jpeg-dir=/usr/lib/ \
    && docker-php-ext-install gd intl pdo_mysql sockets zip \
    && curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && deluser www-data \
    && adduser -D -g 'php user' -h /var/www -s /bin/false -u $uid www-data \
    && mkdir /var/www/.ssh && chown www-data:www-data /var/www/.ssh \
    # xdebug install for local env only
    && set -xe \
    && if [ "$env" = "local" ]; then \
        pecl install xdebug-${xdebug_version} \
        && docker-php-ext-enable xdebug \
        && echo -e "\
zend_extension = xdebug.so \n\
xdebug.remote_enable = On \n\
xdebug.remote_host = ${xdebug_remote_host} \n\
xdebug.remote_port = 9001 \n\
xdebug.idekey = PHPSTORM \n\
" > /usr/local/etc/php/conf.d/xdebug.ini; \
    fi \

    # wkhtmltopdf
    && apk add --no-cache \
       # for getting around this bug https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2037
       xvfb \
       # Additionnal dependencies for better rendering
       ttf-freefont \
       fontconfig \
       dbus \
       qt5-qtbase-dev \
       wkhtmltopdf \

    && mv /usr/bin/wkhtmltopdf /usr/bin/wkhtmltopdf-origin \
    && echo $'#!/usr/bin/env sh\n\
Xvfb :0 -screen 0 1024x768x24 -ac +extension GLX +render -noreset & \n\
DISPLAY=:0.0 wkhtmltopdf-origin $@ \n\
killall Xvfb\
' > /usr/bin/wkhtmltopdf \
    && chmod +x /usr/bin/wkhtmltopdf \

    # Graphviz
    && if [ "$env" = "local" ]; then \
        apk add --no-cache graphviz; \
    fi \

    && apk del --purge alpine-sdk autoconf \
    && rm -rf /usr/src/php.tar* /var/cache/apk/*

COPY ./php.ini /usr/local/etc/php/

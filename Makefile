default: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?##.*$$' $(MAKEFILE_LIST) | sort | awk '{split($$0, a, ":"); printf "\033[36m%-30s\033[0m %-30s %s\n", a[1], a[2], a[3]}'

env?=prod
UID = $(shell id -u)
GID = $(shell id -g)
PHP_ANALYSE_LVL = 7
EXTRA_PARAMS ?=

define run-in-container
	@if [ ! -f /.dockerenv -a "$$(docker-compose ps -q $(2) 2>/dev/null)" ]; then \
		docker-compose exec --user $(1) $(2) /bin/sh -c "$(3)"; \
	elif [ $$(env|grep -c "^CI=") -gt 0 -a $$(env|grep -cw "DOCKER_DRIVER") -eq 1 ]; then \
		docker-compose exec --user $(1) -T $(2) /bin/sh -c "$(3)"; \
	else \
		$(3); \
	fi
endef

define infra-shell
	docker-compose exec -e COLUMNS=`tput cols` -e LINES=`tput lines` $(1)
endef

app-install: ## to install app
	@make app-install-back app-assets-install app-install-front app-cache-clear

app-install-back: ## to install back-end composer dependencies and assets
	mkdir -p var vendor # Prevent composer (on Linux) from creating these directories as root
	$(call run-in-container,www-data,php_fpm,php -d memory_limit=-1 /usr/local/bin/composer install --prefer-dist)
	@make app-cache-clear
	@make app-create-database

app-install-front: ## to install front-end
	$(call run-in-container,root,node,yarn install)
	@make app-assets-compile

app-assets-install: ## install assets as relative symlink
	$(call run-in-container,www-data,php_fpm,bin/console assets:install --symlink --relative public --env=$(env))

app-assets-compile: ## compile front assets
	@if [ ! -d public/assets ]; then mkdir -m 755 public/assets public/assets/{admin,front}; fi
	@$(call run-in-container,root,node,yarn encore $(env))

app-assets-watch: ## watch and compile front assets
	$(call run-in-container,root,node,yarn encore dev --watch)

app-cache-clear: ## to clear the cache
	$(call run-in-container,www-data,php_fpm,rm -rf var/cache/*)
	$(call run-in-container,www-data,php_fpm,php -d memory_limit=-1 bin/console cache:clear --env=$(env))

app-create-database: ## to create the database if it does not exist
	$(call run-in-container,www-data,php_fpm,bin/console doctrine:database:create --if-not-exists --env=$(env))

app-data-reset: ## to empty database and install fixtures
	@$(call run-in-container,www-data,php_fpm,rm -rf public/uploads/* var/cache/*)
	@make app-db-unmigrate app-db-migrate

app-db-migrate: ## Execute all up on database migrations
	@$(call run-in-container,www-data,php_fpm,bin/console doctrine:migrations:migrate --no-interaction --env=$(env))

app-db-unmigrate: ## Execute all down on database migrations
	@$(call run-in-container,www-data,php_fpm,bin/console doctrine:migrations:migrate first --no-interaction --env=$(env))

infra-up: ## to create and start all the containers
	@if [ ! -f .env -a -f .env.dist ]; then sed "s,#UID#,$(UID),g;s,#GID#,$(GID),g" .env.dist > .env; fi
	@docker-compose up -d --build

infra-shell-database: ## open a shell session in the database container
	@$(call infra-shell,database /bin/sh)

infra-shell-php-fpm: ## to open shell session in the php_fpm container
	@$(call infra-shell,--user www-data php_fpm sh)

infra-shell-node: ## open a shell session in the node container
	$(call infra-shell,node bash)

infra-stop: ## to stop all the containers
	@docker-compose stop

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/css/App.scss":
/*!*****************************!*\
  !*** ./assets/css/App.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/Constants/Constants.js":
/*!******************************************!*\
  !*** ./assets/js/Constants/Constants.js ***!
  \******************************************/
/*! exports provided: onUpdateFilesViewEvent, onSelectCategoryEvent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onUpdateFilesViewEvent", function() { return onUpdateFilesViewEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onSelectCategoryEvent", function() { return onSelectCategoryEvent; });
/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/** ===========================
 * Event names.
 * ============================ */
const onUpdateFilesViewEvent = 'onUpdateFilesViewEvent';
const onSelectCategoryEvent = 'onSelectCategoryEvent';

/***/ }),

/***/ "./assets/js/EventHandler/OnSelectCategoryEventHandler.js":
/*!****************************************************************!*\
  !*** ./assets/js/EventHandler/OnSelectCategoryEventHandler.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Helper_EventDispatcher__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Helper/EventDispatcher */ "./assets/js/Helper/EventDispatcher.js");
/* harmony import */ var _Constants_Constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Constants/Constants */ "./assets/js/Constants/Constants.js");
/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


/**
 * This function request the selected category documents
 * and notify the onUpdateFilesViewEvent to update the file lists.
 *
 * @param {object} event
 *
 * @return {void}
 */

/* harmony default export */ __webpack_exports__["default"] = (function (event) {
  const categoryDocumentsRequestURL = event.detail['pathToDocuments'];
  /* Build prepare request. */

  const headers = new Headers();
  const requestInit = {
    method: 'GET',
    headers: headers,
    mode: 'cors',
    cache: 'default'
  };
  const request = new Request(categoryDocumentsRequestURL, requestInit);
  /* Get category documents and notify the onUpdateFilesViewEvent. */

  window.fetch(request, requestInit).then(response => {
    return response.json();
  }).then(response => {
    Object(_Helper_EventDispatcher__WEBPACK_IMPORTED_MODULE_0__["default"])(_Constants_Constants__WEBPACK_IMPORTED_MODULE_1__["onUpdateFilesViewEvent"], response);
  });
});

/***/ }),

/***/ "./assets/js/EventHandler/OnUpdateFilesViewEventHandler.js":
/*!*****************************************************************!*\
  !*** ./assets/js/EventHandler/OnUpdateFilesViewEventHandler.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This function update the selected category label
 * in the top of the documents list.
 *
 * @param {string} categoryName
 *
 * @return {void}
 */
const updateSelectedCategoryLabel = categoryName => {
  const selectedCategoryLabel = document.getElementById('selected-category-label');
  selectedCategoryLabel.innerText = categoryName;
};
/**
 * This function clear the document list in the table.
 *
 * @param {object} selectCategoryDocumentsDOM
 *
 * @return {void}
 */


const clearDocumentListBeforeUpdate = selectCategoryDocumentsDOM => {
  const selectCategoryFileListDOMChildren = document.querySelectorAll('#selected-category-file-list > tr');
  selectCategoryFileListDOMChildren.forEach(child => {
    selectCategoryDocumentsDOM.removeChild(child);
  });
};
/**
 * This function add the document download link in function
 * of current user rights.
 *
 * @param {string} href
 * @param {object} trElement
 * @param {object} selectCategoryDocumentsDOM
 *
 * @return {void}
 */


const addDocumentDownloadColumn = (href, trElement, selectCategoryDocumentsDOM) => {
  const userHasDownloadRights = selectCategoryDocumentsDOM.getAttribute('data-user-has-download-rights');
  const tdElement = document.createElement('td');
  const iElement = document.createElement('i');
  const aElement = document.createElement('a');
  iElement.setAttribute('class', 'fa fa-download');
  iElement.setAttribute('aria-hidden', 'true');

  if (userHasDownloadRights === '1') {
    aElement.setAttribute('href', href);
  } else {
    aElement.setAttribute('href', '#');
    aElement.setAttribute('style', 'pointer-events: none; color: #212529;');
  }

  aElement.appendChild(iElement);
  tdElement.appendChild(aElement);
  trElement.appendChild(tdElement);
};
/**
 * This function generate a new row into the document list table.
 *
 * @param {object} item
 * @param {object} selectCategoryDocumentsDOM
 *
 * @return {void}
 */


const generateDocumentRow = (item, selectCategoryDocumentsDOM) => {
  const trElement = document.createElement('tr');
  /* Iterate on current object to generate the different columns. */

  for (const key in item) {
    /* Exclude the document href. This was added below in function of current user rights. */
    if (key === 'href') {
      continue;
    }
    /* Build the new row column. */


    const tdElement = document.createElement('td');
    tdElement.innerText = item[key];
    trElement.appendChild(tdElement);
  }
  /* Generate document download column. */


  addDocumentDownloadColumn(item['href'], trElement, selectCategoryDocumentsDOM);
  /* Push the new row into the document list. */

  selectCategoryDocumentsDOM.appendChild(trElement);
};
/**
 * This function generate the document list of the selected category.
 *
 * @param {object} event
 *
 * @return {void}
 */


/* harmony default export */ __webpack_exports__["default"] = (function (event) {
  const category = event.detail;
  const selectCategoryDocumentsDOM = document.getElementById('selected-category-file-list');
  /* Update the selected category label in the top of the document list. */

  updateSelectedCategoryLabel(category['name']);
  /* Clear the document list before update if. */

  clearDocumentListBeforeUpdate(selectCategoryDocumentsDOM);
  /* Update the category list indicator. */

  const emptyDocumentListText = document.querySelectorAll('#empty-document-list-text');
  emptyDocumentListText.forEach(element => {
    element.innerHTML = category['documents'].length > 0 ? '' : 'The selected category does not contain associated files.';
  });
  /* Build the document rows. */

  category['documents'].map(document => generateDocumentRow(document, selectCategoryDocumentsDOM));
});

/***/ }),

/***/ "./assets/js/EventListener/BaseEventListener.js":
/*!******************************************************!*\
  !*** ./assets/js/EventListener/BaseEventListener.js ***!
  \******************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EventHandler_OnSelectCategoryEventHandler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../EventHandler/OnSelectCategoryEventHandler */ "./assets/js/EventHandler/OnSelectCategoryEventHandler.js");
/* harmony import */ var _EventHandler_OnUpdateFilesViewEventHandler__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../EventHandler/OnUpdateFilesViewEventHandler */ "./assets/js/EventHandler/OnUpdateFilesViewEventHandler.js");
/* harmony import */ var _Constants_Constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Constants/Constants */ "./assets/js/Constants/Constants.js");
/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */



document.addEventListener(_Constants_Constants__WEBPACK_IMPORTED_MODULE_2__["onSelectCategoryEvent"], _EventHandler_OnSelectCategoryEventHandler__WEBPACK_IMPORTED_MODULE_0__["default"]);
document.addEventListener(_Constants_Constants__WEBPACK_IMPORTED_MODULE_2__["onUpdateFilesViewEvent"], _EventHandler_OnUpdateFilesViewEventHandler__WEBPACK_IMPORTED_MODULE_1__["default"]);

/***/ }),

/***/ "./assets/js/Helper/CategoryTree.js":
/*!******************************************!*\
  !*** ./assets/js/Helper/CategoryTree.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
const toggler = document.getElementsByClassName('caret');

for (let i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener('click', function () {
    this.parentElement.querySelector('.nested').classList.toggle('active');
  });
}

/***/ }),

/***/ "./assets/js/Helper/EventDispatcher.js":
/*!*********************************************!*\
  !*** ./assets/js/Helper/EventDispatcher.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Event Dispatcher helper.
 *
 * @param {string} eventName
 * @param {object} data
 *
 * @return {void}
 */
/* harmony default export */ __webpack_exports__["default"] = (function (eventName, data) {
  const newCustomEvent = new CustomEvent(eventName, {
    'detail': data
  });
  document.dispatchEvent(newCustomEvent);
});

/***/ }),

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var font_awesome_css_font_awesome_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! font-awesome/css/font-awesome.css */ "./node_modules/font-awesome/css/font-awesome.css");
/* harmony import */ var font_awesome_css_font_awesome_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(font_awesome_css_font_awesome_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.css */ "./node_modules/bootstrap/dist/css/bootstrap.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _css_App_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../css/App.scss */ "./assets/css/App.scss");
/* harmony import */ var _css_App_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_css_App_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Helper_CategoryTree__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Helper/CategoryTree */ "./assets/js/Helper/CategoryTree.js");
/* harmony import */ var _Helper_CategoryTree__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Helper_CategoryTree__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _EventListener_BaseEventListener__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./EventListener/BaseEventListener */ "./assets/js/EventListener/BaseEventListener.js");
/*
 * This file is part of the "Documents Manager" project.
 *
 * (c) cRolland
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/** =============================
 *  Import stylesheets
 * ============================== */

/* External */


/* Custom */


/** =============================
 *  Import Javascript Components
 * ============================== */

/* Custom */




/***/ })

},[["./assets/js/app.js","runtime","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL0FwcC5zY3NzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9Db25zdGFudHMvQ29uc3RhbnRzLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9FdmVudEhhbmRsZXIvT25TZWxlY3RDYXRlZ29yeUV2ZW50SGFuZGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvRXZlbnRIYW5kbGVyL09uVXBkYXRlRmlsZXNWaWV3RXZlbnRIYW5kbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9FdmVudExpc3RlbmVyL0Jhc2VFdmVudExpc3RlbmVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9IZWxwZXIvQ2F0ZWdvcnlUcmVlLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9IZWxwZXIvRXZlbnREaXNwYXRjaGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9hcHAuanMiXSwibmFtZXMiOlsib25VcGRhdGVGaWxlc1ZpZXdFdmVudCIsIm9uU2VsZWN0Q2F0ZWdvcnlFdmVudCIsImV2ZW50IiwiY2F0ZWdvcnlEb2N1bWVudHNSZXF1ZXN0VVJMIiwiZGV0YWlsIiwiaGVhZGVycyIsIkhlYWRlcnMiLCJyZXF1ZXN0SW5pdCIsIm1ldGhvZCIsIm1vZGUiLCJjYWNoZSIsInJlcXVlc3QiLCJSZXF1ZXN0Iiwid2luZG93IiwiZmV0Y2giLCJ0aGVuIiwicmVzcG9uc2UiLCJqc29uIiwiZXZlbnREaXNwYXRjaGVyIiwidXBkYXRlU2VsZWN0ZWRDYXRlZ29yeUxhYmVsIiwiY2F0ZWdvcnlOYW1lIiwic2VsZWN0ZWRDYXRlZ29yeUxhYmVsIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsImlubmVyVGV4dCIsImNsZWFyRG9jdW1lbnRMaXN0QmVmb3JlVXBkYXRlIiwic2VsZWN0Q2F0ZWdvcnlEb2N1bWVudHNET00iLCJzZWxlY3RDYXRlZ29yeUZpbGVMaXN0RE9NQ2hpbGRyZW4iLCJxdWVyeVNlbGVjdG9yQWxsIiwiZm9yRWFjaCIsImNoaWxkIiwicmVtb3ZlQ2hpbGQiLCJhZGREb2N1bWVudERvd25sb2FkQ29sdW1uIiwiaHJlZiIsInRyRWxlbWVudCIsInVzZXJIYXNEb3dubG9hZFJpZ2h0cyIsImdldEF0dHJpYnV0ZSIsInRkRWxlbWVudCIsImNyZWF0ZUVsZW1lbnQiLCJpRWxlbWVudCIsImFFbGVtZW50Iiwic2V0QXR0cmlidXRlIiwiYXBwZW5kQ2hpbGQiLCJnZW5lcmF0ZURvY3VtZW50Um93IiwiaXRlbSIsImtleSIsImNhdGVnb3J5IiwiZW1wdHlEb2N1bWVudExpc3RUZXh0IiwiZWxlbWVudCIsImlubmVySFRNTCIsImxlbmd0aCIsIm1hcCIsImFkZEV2ZW50TGlzdGVuZXIiLCJvblNlbGVjdENhdGVnb3J5RXZlbnRIYW5kbGVyIiwib25VcGRhdGVGaWxlc1ZpZXdFdmVudEhhbmRsZXIiLCJ0b2dnbGVyIiwiZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSIsImkiLCJwYXJlbnRFbGVtZW50IiwicXVlcnlTZWxlY3RvciIsImNsYXNzTGlzdCIsInRvZ2dsZSIsImV2ZW50TmFtZSIsImRhdGEiLCJuZXdDdXN0b21FdmVudCIsIkN1c3RvbUV2ZW50IiwiZGlzcGF0Y2hFdmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsdUM7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7QUFTQTs7O0FBSU8sTUFBTUEsc0JBQXNCLEdBQUcsd0JBQS9CO0FBQ0EsTUFBTUMscUJBQXFCLEdBQUcsdUJBQTlCLEM7Ozs7Ozs7Ozs7OztBQ2RQO0FBQUE7QUFBQTtBQUFBOzs7Ozs7OztBQVNBO0FBQ0E7QUFFQTs7Ozs7Ozs7O0FBUWUseUVBQVVDLEtBQVYsRUFBaUI7QUFDNUIsUUFBTUMsMkJBQTJCLEdBQUdELEtBQUssQ0FBQ0UsTUFBTixDQUFhLGlCQUFiLENBQXBDO0FBRUE7O0FBQ0EsUUFBTUMsT0FBTyxHQUFHLElBQUlDLE9BQUosRUFBaEI7QUFDQSxRQUFNQyxXQUFXLEdBQUc7QUFBRUMsVUFBTSxFQUFFLEtBQVY7QUFBaUJILFdBQU8sRUFBRUEsT0FBMUI7QUFBbUNJLFFBQUksRUFBRSxNQUF6QztBQUFpREMsU0FBSyxFQUFFO0FBQXhELEdBQXBCO0FBQ0EsUUFBTUMsT0FBTyxHQUFHLElBQUlDLE9BQUosQ0FBWVQsMkJBQVosRUFBeUNJLFdBQXpDLENBQWhCO0FBRUE7O0FBQ0FNLFFBQU0sQ0FDREMsS0FETCxDQUNXSCxPQURYLEVBQ29CSixXQURwQixFQUVLUSxJQUZMLENBRVdDLFFBQUQsSUFBYztBQUFFLFdBQU9BLFFBQVEsQ0FBQ0MsSUFBVCxFQUFQO0FBQXdCLEdBRmxELEVBR0tGLElBSEwsQ0FHV0MsUUFBRCxJQUFjO0FBQUVFLDJFQUFlLENBQUNsQiwyRUFBRCxFQUF5QmdCLFFBQXpCLENBQWY7QUFBbUQsR0FIN0U7QUFLSCxDOzs7Ozs7Ozs7Ozs7QUNsQ0Q7QUFBQTs7Ozs7Ozs7O0FBU0E7Ozs7Ozs7O0FBUUEsTUFBTUcsMkJBQTJCLEdBQUdDLFlBQVksSUFBSTtBQUNoRCxRQUFNQyxxQkFBcUIsR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLHlCQUF4QixDQUE5QjtBQUNBRix1QkFBcUIsQ0FBQ0csU0FBdEIsR0FBa0NKLFlBQWxDO0FBQ0gsQ0FIRDtBQUtBOzs7Ozs7Ozs7QUFPQSxNQUFNSyw2QkFBNkIsR0FBR0MsMEJBQTBCLElBQUk7QUFDaEUsUUFBTUMsaUNBQWlDLEdBQUdMLFFBQVEsQ0FBQ00sZ0JBQVQsQ0FBMEIsbUNBQTFCLENBQTFDO0FBRUFELG1DQUFpQyxDQUFDRSxPQUFsQyxDQUEwQ0MsS0FBSyxJQUFJO0FBQy9DSiw4QkFBMEIsQ0FBQ0ssV0FBM0IsQ0FBdUNELEtBQXZDO0FBQ0gsR0FGRDtBQUdILENBTkQ7QUFRQTs7Ozs7Ozs7Ozs7O0FBVUEsTUFBTUUseUJBQXlCLEdBQUcsQ0FBQ0MsSUFBRCxFQUFPQyxTQUFQLEVBQWtCUiwwQkFBbEIsS0FBaUQ7QUFDL0UsUUFBTVMscUJBQXFCLEdBQUdULDBCQUEwQixDQUFDVSxZQUEzQixDQUF3QywrQkFBeEMsQ0FBOUI7QUFFQSxRQUFNQyxTQUFTLEdBQUdmLFFBQVEsQ0FBQ2dCLGFBQVQsQ0FBdUIsSUFBdkIsQ0FBbEI7QUFDQSxRQUFNQyxRQUFRLEdBQUdqQixRQUFRLENBQUNnQixhQUFULENBQXVCLEdBQXZCLENBQWpCO0FBQ0EsUUFBTUUsUUFBUSxHQUFHbEIsUUFBUSxDQUFDZ0IsYUFBVCxDQUF1QixHQUF2QixDQUFqQjtBQUVBQyxVQUFRLENBQUNFLFlBQVQsQ0FBc0IsT0FBdEIsRUFBK0IsZ0JBQS9CO0FBQ0FGLFVBQVEsQ0FBQ0UsWUFBVCxDQUFzQixhQUF0QixFQUFxQyxNQUFyQzs7QUFFQSxNQUFJTixxQkFBcUIsS0FBSyxHQUE5QixFQUFtQztBQUMvQkssWUFBUSxDQUFDQyxZQUFULENBQXNCLE1BQXRCLEVBQThCUixJQUE5QjtBQUNILEdBRkQsTUFFTztBQUNITyxZQUFRLENBQUNDLFlBQVQsQ0FBc0IsTUFBdEIsRUFBOEIsR0FBOUI7QUFDQUQsWUFBUSxDQUFDQyxZQUFULENBQXNCLE9BQXRCLEVBQStCLHVDQUEvQjtBQUNIOztBQUVERCxVQUFRLENBQUNFLFdBQVQsQ0FBcUJILFFBQXJCO0FBQ0FGLFdBQVMsQ0FBQ0ssV0FBVixDQUFzQkYsUUFBdEI7QUFDQU4sV0FBUyxDQUFDUSxXQUFWLENBQXNCTCxTQUF0QjtBQUNILENBcEJEO0FBc0JBOzs7Ozs7Ozs7O0FBUUEsTUFBTU0sbUJBQW1CLEdBQUcsQ0FBQ0MsSUFBRCxFQUFPbEIsMEJBQVAsS0FBc0M7QUFDOUQsUUFBTVEsU0FBUyxHQUFHWixRQUFRLENBQUNnQixhQUFULENBQXVCLElBQXZCLENBQWxCO0FBRUE7O0FBQ0EsT0FBSyxNQUFNTyxHQUFYLElBQWtCRCxJQUFsQixFQUF3QjtBQUVwQjtBQUNBLFFBQUlDLEdBQUcsS0FBSyxNQUFaLEVBQW9CO0FBQ2hCO0FBQ0g7QUFFRDs7O0FBQ0EsVUFBTVIsU0FBUyxHQUFHZixRQUFRLENBQUNnQixhQUFULENBQXVCLElBQXZCLENBQWxCO0FBQ0FELGFBQVMsQ0FBQ2IsU0FBVixHQUFzQm9CLElBQUksQ0FBQ0MsR0FBRCxDQUExQjtBQUVBWCxhQUFTLENBQUNRLFdBQVYsQ0FBc0JMLFNBQXRCO0FBQ0g7QUFFRDs7O0FBQ0FMLDJCQUF5QixDQUFDWSxJQUFJLENBQUMsTUFBRCxDQUFMLEVBQWVWLFNBQWYsRUFBMEJSLDBCQUExQixDQUF6QjtBQUVBOztBQUNBQSw0QkFBMEIsQ0FBQ2dCLFdBQTNCLENBQXVDUixTQUF2QztBQUNILENBdkJEO0FBeUJBOzs7Ozs7Ozs7QUFPZSx5RUFBVWhDLEtBQVYsRUFBaUI7QUFDNUIsUUFBTTRDLFFBQVEsR0FBRzVDLEtBQUssQ0FBQ0UsTUFBdkI7QUFDQSxRQUFNc0IsMEJBQTBCLEdBQUdKLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3Qiw2QkFBeEIsQ0FBbkM7QUFFQTs7QUFDQUosNkJBQTJCLENBQUMyQixRQUFRLENBQUMsTUFBRCxDQUFULENBQTNCO0FBRUE7O0FBQ0FyQiwrQkFBNkIsQ0FBQ0MsMEJBQUQsQ0FBN0I7QUFFQTs7QUFDQSxRQUFNcUIscUJBQXFCLEdBQUd6QixRQUFRLENBQUNNLGdCQUFULENBQTBCLDJCQUExQixDQUE5QjtBQUNBbUIsdUJBQXFCLENBQUNsQixPQUF0QixDQUE4Qm1CLE9BQU8sSUFBSTtBQUNyQ0EsV0FBTyxDQUFDQyxTQUFSLEdBQW9CSCxRQUFRLENBQUMsV0FBRCxDQUFSLENBQXNCSSxNQUF0QixHQUErQixDQUEvQixHQUFtQyxFQUFuQyxHQUF3QywwREFBNUQ7QUFDSCxHQUZEO0FBSUE7O0FBQ0FKLFVBQVEsQ0FBQyxXQUFELENBQVIsQ0FBc0JLLEdBQXRCLENBQTBCN0IsUUFBUSxJQUFJcUIsbUJBQW1CLENBQUNyQixRQUFELEVBQVdJLDBCQUFYLENBQXpEO0FBQ0gsQzs7Ozs7Ozs7Ozs7O0FDL0hEO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBRUFKLFFBQVEsQ0FBQzhCLGdCQUFULENBQTBCbkQsMEVBQTFCLEVBQWlEb0Qsa0ZBQWpEO0FBQ0EvQixRQUFRLENBQUM4QixnQkFBVCxDQUEwQnBELDJFQUExQixFQUFrRHNELG1GQUFsRCxFOzs7Ozs7Ozs7OztBQ2RBOzs7Ozs7OztBQVNBLE1BQU1DLE9BQU8sR0FBR2pDLFFBQVEsQ0FBQ2tDLHNCQUFULENBQWdDLE9BQWhDLENBQWhCOztBQUVBLEtBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0YsT0FBTyxDQUFDTCxNQUE1QixFQUFvQ08sQ0FBQyxFQUFyQyxFQUF5QztBQUNyQ0YsU0FBTyxDQUFDRSxDQUFELENBQVAsQ0FBV0wsZ0JBQVgsQ0FBNEIsT0FBNUIsRUFBcUMsWUFBVztBQUM1QyxTQUFLTSxhQUFMLENBQW1CQyxhQUFuQixDQUFpQyxTQUFqQyxFQUE0Q0MsU0FBNUMsQ0FBc0RDLE1BQXRELENBQTZELFFBQTdEO0FBQ0gsR0FGRDtBQUdILEM7Ozs7Ozs7Ozs7OztBQ2ZEO0FBQUE7Ozs7Ozs7OztBQVNBOzs7Ozs7OztBQVFlLHlFQUFVQyxTQUFWLEVBQXFCQyxJQUFyQixFQUEyQjtBQUN0QyxRQUFNQyxjQUFjLEdBQUcsSUFBSUMsV0FBSixDQUFnQkgsU0FBaEIsRUFBMkI7QUFBQyxjQUFVQztBQUFYLEdBQTNCLENBQXZCO0FBQ0F6QyxVQUFRLENBQUM0QyxhQUFULENBQXVCRixjQUF2QjtBQUNILEM7Ozs7Ozs7Ozs7OztBQ3BCRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7QUFTQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUVBOztBQUNBO0FBRUE7Ozs7QUFJQTs7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCIvKlxuICogVGhpcyBmaWxlIGlzIHBhcnQgb2YgdGhlIFwiRG9jdW1lbnRzIE1hbmFnZXJcIiBwcm9qZWN0LlxuICpcbiAqIChjKSBjUm9sbGFuZFxuICpcbiAqIEZvciB0aGUgZnVsbCBjb3B5cmlnaHQgYW5kIGxpY2Vuc2UgaW5mb3JtYXRpb24sIHBsZWFzZSB2aWV3IHRoZSBMSUNFTlNFXG4gKiBmaWxlIHRoYXQgd2FzIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyBzb3VyY2UgY29kZS5cbiAqL1xuXG4vKiogPT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiBFdmVudCBuYW1lcy5cbiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuZXhwb3J0IGNvbnN0IG9uVXBkYXRlRmlsZXNWaWV3RXZlbnQgPSAnb25VcGRhdGVGaWxlc1ZpZXdFdmVudCc7XG5leHBvcnQgY29uc3Qgb25TZWxlY3RDYXRlZ29yeUV2ZW50ID0gJ29uU2VsZWN0Q2F0ZWdvcnlFdmVudCc7XG4iLCIvKlxuICogVGhpcyBmaWxlIGlzIHBhcnQgb2YgdGhlIFwiRG9jdW1lbnRzIE1hbmFnZXJcIiBwcm9qZWN0LlxuICpcbiAqIChjKSBjUm9sbGFuZFxuICpcbiAqIEZvciB0aGUgZnVsbCBjb3B5cmlnaHQgYW5kIGxpY2Vuc2UgaW5mb3JtYXRpb24sIHBsZWFzZSB2aWV3IHRoZSBMSUNFTlNFXG4gKiBmaWxlIHRoYXQgd2FzIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyBzb3VyY2UgY29kZS5cbiAqL1xuXG5pbXBvcnQgZXZlbnREaXNwYXRjaGVyIGZyb20gJy4uL0hlbHBlci9FdmVudERpc3BhdGNoZXInXG5pbXBvcnQgeyBvblVwZGF0ZUZpbGVzVmlld0V2ZW50IH0gZnJvbSAnLi4vQ29uc3RhbnRzL0NvbnN0YW50cydcblxuLyoqXG4gKiBUaGlzIGZ1bmN0aW9uIHJlcXVlc3QgdGhlIHNlbGVjdGVkIGNhdGVnb3J5IGRvY3VtZW50c1xuICogYW5kIG5vdGlmeSB0aGUgb25VcGRhdGVGaWxlc1ZpZXdFdmVudCB0byB1cGRhdGUgdGhlIGZpbGUgbGlzdHMuXG4gKlxuICogQHBhcmFtIHtvYmplY3R9IGV2ZW50XG4gKlxuICogQHJldHVybiB7dm9pZH1cbiAqL1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgY29uc3QgY2F0ZWdvcnlEb2N1bWVudHNSZXF1ZXN0VVJMID0gZXZlbnQuZGV0YWlsWydwYXRoVG9Eb2N1bWVudHMnXTtcblxuICAgIC8qIEJ1aWxkIHByZXBhcmUgcmVxdWVzdC4gKi9cbiAgICBjb25zdCBoZWFkZXJzID0gbmV3IEhlYWRlcnMoKTtcbiAgICBjb25zdCByZXF1ZXN0SW5pdCA9IHsgbWV0aG9kOiAnR0VUJywgaGVhZGVyczogaGVhZGVycywgbW9kZTogJ2NvcnMnLCBjYWNoZTogJ2RlZmF1bHQnIH07XG4gICAgY29uc3QgcmVxdWVzdCA9IG5ldyBSZXF1ZXN0KGNhdGVnb3J5RG9jdW1lbnRzUmVxdWVzdFVSTCwgcmVxdWVzdEluaXQpO1xuXG4gICAgLyogR2V0IGNhdGVnb3J5IGRvY3VtZW50cyBhbmQgbm90aWZ5IHRoZSBvblVwZGF0ZUZpbGVzVmlld0V2ZW50LiAqL1xuICAgIHdpbmRvd1xuICAgICAgICAuZmV0Y2gocmVxdWVzdCwgcmVxdWVzdEluaXQpXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4geyByZXR1cm4gcmVzcG9uc2UuanNvbigpIH0pXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4geyBldmVudERpc3BhdGNoZXIob25VcGRhdGVGaWxlc1ZpZXdFdmVudCwgcmVzcG9uc2UpIH0pXG4gICAgO1xufVxuIiwiLypcbiAqIFRoaXMgZmlsZSBpcyBwYXJ0IG9mIHRoZSBcIkRvY3VtZW50cyBNYW5hZ2VyXCIgcHJvamVjdC5cbiAqXG4gKiAoYykgY1JvbGxhbmRcbiAqXG4gKiBGb3IgdGhlIGZ1bGwgY29weXJpZ2h0IGFuZCBsaWNlbnNlIGluZm9ybWF0aW9uLCBwbGVhc2UgdmlldyB0aGUgTElDRU5TRVxuICogZmlsZSB0aGF0IHdhcyBkaXN0cmlidXRlZCB3aXRoIHRoaXMgc291cmNlIGNvZGUuXG4gKi9cblxuLyoqXG4gKiBUaGlzIGZ1bmN0aW9uIHVwZGF0ZSB0aGUgc2VsZWN0ZWQgY2F0ZWdvcnkgbGFiZWxcbiAqIGluIHRoZSB0b3Agb2YgdGhlIGRvY3VtZW50cyBsaXN0LlxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBjYXRlZ29yeU5hbWVcbiAqXG4gKiBAcmV0dXJuIHt2b2lkfVxuICovXG5jb25zdCB1cGRhdGVTZWxlY3RlZENhdGVnb3J5TGFiZWwgPSBjYXRlZ29yeU5hbWUgPT4ge1xuICAgIGNvbnN0IHNlbGVjdGVkQ2F0ZWdvcnlMYWJlbCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzZWxlY3RlZC1jYXRlZ29yeS1sYWJlbCcpO1xuICAgIHNlbGVjdGVkQ2F0ZWdvcnlMYWJlbC5pbm5lclRleHQgPSBjYXRlZ29yeU5hbWU7XG59O1xuXG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gY2xlYXIgdGhlIGRvY3VtZW50IGxpc3QgaW4gdGhlIHRhYmxlLlxuICpcbiAqIEBwYXJhbSB7b2JqZWN0fSBzZWxlY3RDYXRlZ29yeURvY3VtZW50c0RPTVxuICpcbiAqIEByZXR1cm4ge3ZvaWR9XG4gKi9cbmNvbnN0IGNsZWFyRG9jdW1lbnRMaXN0QmVmb3JlVXBkYXRlID0gc2VsZWN0Q2F0ZWdvcnlEb2N1bWVudHNET00gPT4ge1xuICAgIGNvbnN0IHNlbGVjdENhdGVnb3J5RmlsZUxpc3RET01DaGlsZHJlbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJyNzZWxlY3RlZC1jYXRlZ29yeS1maWxlLWxpc3QgPiB0cicpO1xuXG4gICAgc2VsZWN0Q2F0ZWdvcnlGaWxlTGlzdERPTUNoaWxkcmVuLmZvckVhY2goY2hpbGQgPT4ge1xuICAgICAgICBzZWxlY3RDYXRlZ29yeURvY3VtZW50c0RPTS5yZW1vdmVDaGlsZChjaGlsZCk7XG4gICAgfSk7XG59O1xuXG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gYWRkIHRoZSBkb2N1bWVudCBkb3dubG9hZCBsaW5rIGluIGZ1bmN0aW9uXG4gKiBvZiBjdXJyZW50IHVzZXIgcmlnaHRzLlxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBocmVmXG4gKiBAcGFyYW0ge29iamVjdH0gdHJFbGVtZW50XG4gKiBAcGFyYW0ge29iamVjdH0gc2VsZWN0Q2F0ZWdvcnlEb2N1bWVudHNET01cbiAqXG4gKiBAcmV0dXJuIHt2b2lkfVxuICovXG5jb25zdCBhZGREb2N1bWVudERvd25sb2FkQ29sdW1uID0gKGhyZWYsIHRyRWxlbWVudCwgc2VsZWN0Q2F0ZWdvcnlEb2N1bWVudHNET00pID0+IHtcbiAgICBjb25zdCB1c2VySGFzRG93bmxvYWRSaWdodHMgPSBzZWxlY3RDYXRlZ29yeURvY3VtZW50c0RPTS5nZXRBdHRyaWJ1dGUoJ2RhdGEtdXNlci1oYXMtZG93bmxvYWQtcmlnaHRzJyk7XG5cbiAgICBjb25zdCB0ZEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd0ZCcpO1xuICAgIGNvbnN0IGlFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaScpO1xuICAgIGNvbnN0IGFFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xuXG4gICAgaUVsZW1lbnQuc2V0QXR0cmlidXRlKCdjbGFzcycsICdmYSBmYS1kb3dubG9hZCcpO1xuICAgIGlFbGVtZW50LnNldEF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nLCAndHJ1ZScpO1xuXG4gICAgaWYgKHVzZXJIYXNEb3dubG9hZFJpZ2h0cyA9PT0gJzEnKSB7XG4gICAgICAgIGFFbGVtZW50LnNldEF0dHJpYnV0ZSgnaHJlZicsIGhyZWYpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGFFbGVtZW50LnNldEF0dHJpYnV0ZSgnaHJlZicsICcjJyk7XG4gICAgICAgIGFFbGVtZW50LnNldEF0dHJpYnV0ZSgnc3R5bGUnLCAncG9pbnRlci1ldmVudHM6IG5vbmU7IGNvbG9yOiAjMjEyNTI5OycpXG4gICAgfVxuXG4gICAgYUVsZW1lbnQuYXBwZW5kQ2hpbGQoaUVsZW1lbnQpO1xuICAgIHRkRWxlbWVudC5hcHBlbmRDaGlsZChhRWxlbWVudCk7XG4gICAgdHJFbGVtZW50LmFwcGVuZENoaWxkKHRkRWxlbWVudCk7XG59O1xuXG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gZ2VuZXJhdGUgYSBuZXcgcm93IGludG8gdGhlIGRvY3VtZW50IGxpc3QgdGFibGUuXG4gKlxuICogQHBhcmFtIHtvYmplY3R9IGl0ZW1cbiAqIEBwYXJhbSB7b2JqZWN0fSBzZWxlY3RDYXRlZ29yeURvY3VtZW50c0RPTVxuICpcbiAqIEByZXR1cm4ge3ZvaWR9XG4gKi9cbmNvbnN0IGdlbmVyYXRlRG9jdW1lbnRSb3cgPSAoaXRlbSwgc2VsZWN0Q2F0ZWdvcnlEb2N1bWVudHNET00pID0+IHtcbiAgICBjb25zdCB0ckVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd0cicpO1xuXG4gICAgLyogSXRlcmF0ZSBvbiBjdXJyZW50IG9iamVjdCB0byBnZW5lcmF0ZSB0aGUgZGlmZmVyZW50IGNvbHVtbnMuICovXG4gICAgZm9yIChjb25zdCBrZXkgaW4gaXRlbSkge1xuXG4gICAgICAgIC8qIEV4Y2x1ZGUgdGhlIGRvY3VtZW50IGhyZWYuIFRoaXMgd2FzIGFkZGVkIGJlbG93IGluIGZ1bmN0aW9uIG9mIGN1cnJlbnQgdXNlciByaWdodHMuICovXG4gICAgICAgIGlmIChrZXkgPT09ICdocmVmJykge1xuICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgIH1cblxuICAgICAgICAvKiBCdWlsZCB0aGUgbmV3IHJvdyBjb2x1bW4uICovXG4gICAgICAgIGNvbnN0IHRkRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3RkJyk7XG4gICAgICAgIHRkRWxlbWVudC5pbm5lclRleHQgPSBpdGVtW2tleV07XG5cbiAgICAgICAgdHJFbGVtZW50LmFwcGVuZENoaWxkKHRkRWxlbWVudCk7XG4gICAgfVxuXG4gICAgLyogR2VuZXJhdGUgZG9jdW1lbnQgZG93bmxvYWQgY29sdW1uLiAqL1xuICAgIGFkZERvY3VtZW50RG93bmxvYWRDb2x1bW4oaXRlbVsnaHJlZiddLCB0ckVsZW1lbnQsIHNlbGVjdENhdGVnb3J5RG9jdW1lbnRzRE9NKTtcblxuICAgIC8qIFB1c2ggdGhlIG5ldyByb3cgaW50byB0aGUgZG9jdW1lbnQgbGlzdC4gKi9cbiAgICBzZWxlY3RDYXRlZ29yeURvY3VtZW50c0RPTS5hcHBlbmRDaGlsZCh0ckVsZW1lbnQpO1xufTtcblxuLyoqXG4gKiBUaGlzIGZ1bmN0aW9uIGdlbmVyYXRlIHRoZSBkb2N1bWVudCBsaXN0IG9mIHRoZSBzZWxlY3RlZCBjYXRlZ29yeS5cbiAqXG4gKiBAcGFyYW0ge29iamVjdH0gZXZlbnRcbiAqXG4gKiBAcmV0dXJuIHt2b2lkfVxuICovXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBjb25zdCBjYXRlZ29yeSA9IGV2ZW50LmRldGFpbDtcbiAgICBjb25zdCBzZWxlY3RDYXRlZ29yeURvY3VtZW50c0RPTSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzZWxlY3RlZC1jYXRlZ29yeS1maWxlLWxpc3QnKTtcblxuICAgIC8qIFVwZGF0ZSB0aGUgc2VsZWN0ZWQgY2F0ZWdvcnkgbGFiZWwgaW4gdGhlIHRvcCBvZiB0aGUgZG9jdW1lbnQgbGlzdC4gKi9cbiAgICB1cGRhdGVTZWxlY3RlZENhdGVnb3J5TGFiZWwoY2F0ZWdvcnlbJ25hbWUnXSk7XG5cbiAgICAvKiBDbGVhciB0aGUgZG9jdW1lbnQgbGlzdCBiZWZvcmUgdXBkYXRlIGlmLiAqL1xuICAgIGNsZWFyRG9jdW1lbnRMaXN0QmVmb3JlVXBkYXRlKHNlbGVjdENhdGVnb3J5RG9jdW1lbnRzRE9NKTtcblxuICAgIC8qIFVwZGF0ZSB0aGUgY2F0ZWdvcnkgbGlzdCBpbmRpY2F0b3IuICovXG4gICAgY29uc3QgZW1wdHlEb2N1bWVudExpc3RUZXh0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnI2VtcHR5LWRvY3VtZW50LWxpc3QtdGV4dCcpO1xuICAgIGVtcHR5RG9jdW1lbnRMaXN0VGV4dC5mb3JFYWNoKGVsZW1lbnQgPT4ge1xuICAgICAgICBlbGVtZW50LmlubmVySFRNTCA9IGNhdGVnb3J5Wydkb2N1bWVudHMnXS5sZW5ndGggPiAwID8gJycgOiAnVGhlIHNlbGVjdGVkIGNhdGVnb3J5IGRvZXMgbm90IGNvbnRhaW4gYXNzb2NpYXRlZCBmaWxlcy4nO1xuICAgIH0pO1xuXG4gICAgLyogQnVpbGQgdGhlIGRvY3VtZW50IHJvd3MuICovXG4gICAgY2F0ZWdvcnlbJ2RvY3VtZW50cyddLm1hcChkb2N1bWVudCA9PiBnZW5lcmF0ZURvY3VtZW50Um93KGRvY3VtZW50LCBzZWxlY3RDYXRlZ29yeURvY3VtZW50c0RPTSkpO1xufVxuIiwiLypcbiAqIFRoaXMgZmlsZSBpcyBwYXJ0IG9mIHRoZSBcIkRvY3VtZW50cyBNYW5hZ2VyXCIgcHJvamVjdC5cbiAqXG4gKiAoYykgY1JvbGxhbmRcbiAqXG4gKiBGb3IgdGhlIGZ1bGwgY29weXJpZ2h0IGFuZCBsaWNlbnNlIGluZm9ybWF0aW9uLCBwbGVhc2UgdmlldyB0aGUgTElDRU5TRVxuICogZmlsZSB0aGF0IHdhcyBkaXN0cmlidXRlZCB3aXRoIHRoaXMgc291cmNlIGNvZGUuXG4gKi9cblxuaW1wb3J0IG9uU2VsZWN0Q2F0ZWdvcnlFdmVudEhhbmRsZXIgZnJvbSAnLi4vRXZlbnRIYW5kbGVyL09uU2VsZWN0Q2F0ZWdvcnlFdmVudEhhbmRsZXInXG5pbXBvcnQgb25VcGRhdGVGaWxlc1ZpZXdFdmVudEhhbmRsZXIgZnJvbSAnLi4vRXZlbnRIYW5kbGVyL09uVXBkYXRlRmlsZXNWaWV3RXZlbnRIYW5kbGVyJ1xuaW1wb3J0IHsgb25TZWxlY3RDYXRlZ29yeUV2ZW50LCBvblVwZGF0ZUZpbGVzVmlld0V2ZW50IH0gZnJvbSAnLi4vQ29uc3RhbnRzL0NvbnN0YW50cydcblxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihvblNlbGVjdENhdGVnb3J5RXZlbnQsIG9uU2VsZWN0Q2F0ZWdvcnlFdmVudEhhbmRsZXIpO1xuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihvblVwZGF0ZUZpbGVzVmlld0V2ZW50LCBvblVwZGF0ZUZpbGVzVmlld0V2ZW50SGFuZGxlcik7XG4iLCIvKlxuICogVGhpcyBmaWxlIGlzIHBhcnQgb2YgdGhlIFwiRG9jdW1lbnRzIE1hbmFnZXJcIiBwcm9qZWN0LlxuICpcbiAqIChjKSBjUm9sbGFuZFxuICpcbiAqIEZvciB0aGUgZnVsbCBjb3B5cmlnaHQgYW5kIGxpY2Vuc2UgaW5mb3JtYXRpb24sIHBsZWFzZSB2aWV3IHRoZSBMSUNFTlNFXG4gKiBmaWxlIHRoYXQgd2FzIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyBzb3VyY2UgY29kZS5cbiAqL1xuXG5jb25zdCB0b2dnbGVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnY2FyZXQnKTtcblxuZm9yIChsZXQgaSA9IDA7IGkgPCB0b2dnbGVyLmxlbmd0aDsgaSsrKSB7XG4gICAgdG9nZ2xlcltpXS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgICB0aGlzLnBhcmVudEVsZW1lbnQucXVlcnlTZWxlY3RvcignLm5lc3RlZCcpLmNsYXNzTGlzdC50b2dnbGUoJ2FjdGl2ZScpO1xuICAgIH0pO1xufVxuIiwiLypcbiAqIFRoaXMgZmlsZSBpcyBwYXJ0IG9mIHRoZSBcIkRvY3VtZW50cyBNYW5hZ2VyXCIgcHJvamVjdC5cbiAqXG4gKiAoYykgY1JvbGxhbmRcbiAqXG4gKiBGb3IgdGhlIGZ1bGwgY29weXJpZ2h0IGFuZCBsaWNlbnNlIGluZm9ybWF0aW9uLCBwbGVhc2UgdmlldyB0aGUgTElDRU5TRVxuICogZmlsZSB0aGF0IHdhcyBkaXN0cmlidXRlZCB3aXRoIHRoaXMgc291cmNlIGNvZGUuXG4gKi9cblxuLyoqXG4gKiBFdmVudCBEaXNwYXRjaGVyIGhlbHBlci5cbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gZXZlbnROYW1lXG4gKiBAcGFyYW0ge29iamVjdH0gZGF0YVxuICpcbiAqIEByZXR1cm4ge3ZvaWR9XG4gKi9cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIChldmVudE5hbWUsIGRhdGEpIHtcbiAgICBjb25zdCBuZXdDdXN0b21FdmVudCA9IG5ldyBDdXN0b21FdmVudChldmVudE5hbWUsIHsnZGV0YWlsJzogZGF0YX0pO1xuICAgIGRvY3VtZW50LmRpc3BhdGNoRXZlbnQobmV3Q3VzdG9tRXZlbnQpO1xufVxuIiwiLypcbiAqIFRoaXMgZmlsZSBpcyBwYXJ0IG9mIHRoZSBcIkRvY3VtZW50cyBNYW5hZ2VyXCIgcHJvamVjdC5cbiAqXG4gKiAoYykgY1JvbGxhbmRcbiAqXG4gKiBGb3IgdGhlIGZ1bGwgY29weXJpZ2h0IGFuZCBsaWNlbnNlIGluZm9ybWF0aW9uLCBwbGVhc2UgdmlldyB0aGUgTElDRU5TRVxuICogZmlsZSB0aGF0IHdhcyBkaXN0cmlidXRlZCB3aXRoIHRoaXMgc291cmNlIGNvZGUuXG4gKi9cblxuLyoqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiAgSW1wb3J0IHN0eWxlc2hlZXRzXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuLyogRXh0ZXJuYWwgKi9cbmltcG9ydCAnZm9udC1hd2Vzb21lL2Nzcy9mb250LWF3ZXNvbWUuY3NzJ1xuaW1wb3J0ICdib290c3RyYXAvZGlzdC9jc3MvYm9vdHN0cmFwLmNzcydcblxuLyogQ3VzdG9tICovXG5pbXBvcnQgJy4uL2Nzcy9BcHAuc2NzcydcblxuLyoqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gKiAgSW1wb3J0IEphdmFzY3JpcHQgQ29tcG9uZW50c1xuICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXG5cbi8qIEN1c3RvbSAqL1xuaW1wb3J0ICcuL0hlbHBlci9DYXRlZ29yeVRyZWUnXG5pbXBvcnQgJy4vRXZlbnRMaXN0ZW5lci9CYXNlRXZlbnRMaXN0ZW5lcic7XG4iXSwic291cmNlUm9vdCI6IiJ9